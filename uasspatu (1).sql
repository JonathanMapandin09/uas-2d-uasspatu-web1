-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2020 at 12:45 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uasspatu`
--

-- --------------------------------------------------------

--
-- Table structure for table `brandspatu`
--

CREATE TABLE `brandspatu` (
  `id_brand` int(10) NOT NULL,
  `brand` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brandspatu`
--

INSERT INTO `brandspatu` (`id_brand`, `brand`) VALUES
(1, 'Import'),
(2, 'Lokal');

-- --------------------------------------------------------

--
-- Table structure for table `daftarspatu`
--

CREATE TABLE `daftarspatu` (
  `id_spatu` varchar(10) NOT NULL,
  `nama_spatu` varchar(30) NOT NULL,
  `id_merk` int(11) NOT NULL,
  `photos` varchar(50) DEFAULT NULL,
  `harga` varchar(30) NOT NULL,
  `gender_spatu` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daftarspatu`
--

INSERT INTO `daftarspatu` (`id_spatu`, `nama_spatu`, `id_merk`, `photos`, `harga`, `gender_spatu`) VALUES
('ADI005', 'Superstar Shoes', 8, 'Superstar Shoes.jpg', 'Rp 1.550.000', 'Pria'),
('CON004', 'Chuck Taylor All Star', 7, 'Chuck Taylor All Star.jpg', 'Rp.850.000', 'Wanita'),
('CON008', 'Chuck Taylor All Star High Top', 7, 'Chuck Taylor All Star High Top.jpg', 'Rp 900.000', 'Pria'),
('NIK003', 'Air Max 2090', 6, 'Air Max 2090.jpg', 'Rp 1.500.000', 'Pria'),
('NIK006', 'Tanjun', 6, 'Tanjun.jpg', 'Rp.1.200.000', 'Wanita'),
('YEZ002', 'Boots 350 V2', 5, 'Boots 350 V2.jpg', 'Rp 1.300.000', 'Pria');

-- --------------------------------------------------------

--
-- Table structure for table `merkspatu`
--

CREATE TABLE `merkspatu` (
  `id_merk` int(11) NOT NULL,
  `nama_merk` varchar(30) NOT NULL,
  `id_brand` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `merkspatu`
--

INSERT INTO `merkspatu` (`id_merk`, `nama_merk`, `id_brand`) VALUES
(5, 'Yeezy', 1),
(6, 'Nike', 1),
(7, 'Converse', 2),
(8, 'Adidas', 1),
(9, 'Vans', 2),
(10, 'Superga', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brandspatu`
--
ALTER TABLE `brandspatu`
  ADD PRIMARY KEY (`id_brand`);

--
-- Indexes for table `daftarspatu`
--
ALTER TABLE `daftarspatu`
  ADD PRIMARY KEY (`id_spatu`);

--
-- Indexes for table `merkspatu`
--
ALTER TABLE `merkspatu`
  ADD PRIMARY KEY (`id_merk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brandspatu`
--
ALTER TABLE `brandspatu`
  MODIFY `id_brand` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `merkspatu`
--
ALTER TABLE `merkspatu`
  MODIFY `id_merk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
